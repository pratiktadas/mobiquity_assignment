import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import { WeatherService } from '../services/weather.service';

@Component({
  selector: 'app-cities',
  templateUrl: './cities.component.html',
  styleUrls: ['./cities.component.css']
})
export class CitiesComponent implements OnInit {

  predefined_cities: any = [
    'Paris',
    'London',
    'Rome',
    'Barcelona',
    'Amsterdam'
  ];

  cities: any = [];

  constructor(
    private weatherService: WeatherService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.predefined_cities.forEach(element => {
      this.weatherService.getCitiesWithWeatherByName(element).subscribe((res: any) => {
        let sunrise_time = new Date(res.sys.sunrise * 1000);
        let sunset_time = new Date(res.sys.sunset * 1000);
        let sunrise_suffix = (sunrise_time.getHours() >= 12)? 'PM' : 'AM';
        let sunset_suffix = (sunrise_time.getHours() >= 12)? 'PM' : 'AM';
        res['sunrise_time'] = `${sunrise_time.toJSON().substring(11, 19)} ${sunrise_suffix}`;
        res['sunset_time'] = `${sunset_time.toJSON().substring(11, 19)} ${sunset_suffix}`;
        this.cities.push(res);
      }, error => {
        console.log("Sometihng Went Wrong", error);
      })
    });
  }

  onCityClick = (city_id) => {
    console.log("city_id", city_id);
    this.router.navigate(['city', city_id ]);
  }

}
