import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CitiesComponent } from './cities.component';
import { WeatherService } from '../services/weather.service';

describe('CitiesComponent', () => {
  let component: CitiesComponent;
  let fixture: ComponentFixture<CitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CitiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create Cities Component', () => {
    expect(component).toBeTruthy();
  });

  // it('should fetch five cities weather information of europe continent', () => {
  //   const weatherService = fixture.debugElement.injector.get(WeatherService);
  //   fixture.detectChanges();
  //   expect(weatherService.getCitiesWithWeatherByName(2988507)).toEqual(component.cities);
  // })
});
