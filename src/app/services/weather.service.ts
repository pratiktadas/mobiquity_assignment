import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  app_id: any;
  units: String = 'metric';

  constructor(private http: HttpClient) { 
    this.app_id = '3d8b309701a13f65b660fa2c64cdc517'
  }

  getCitiesWithWeatherByName = (city_name) => {
    return this.http.get(`http://api.openweathermap.org/data/2.5/weather?q=${city_name}&units=${this.units}&appid=${this.app_id}`);
  }

  getCityForecastById = (city_id) => {
    return this.http.get(`http://api.openweathermap.org/data/2.5/forecast?id=${city_id}&appid=${this.app_id}&units=${this.units}`);
  }

}
