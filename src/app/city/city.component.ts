import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { WeatherService } from '../services/weather.service';

@Component({
  selector: 'app-city',
  templateUrl: './city.component.html',
  styleUrls: ['./city.component.css']
})
export class CityComponent implements OnInit {

  city_forecast: any = {};
  weekdays = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
  
  constructor(
    private weatherService: WeatherService,  
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      console.log("params", params.city_id);
      if (params.city_id) {
        this.getforecast(params.city_id);
      } else {
        this.router.navigate(['cities']);
      }
    });
  }

  getforecast = (city_id) => {
    this.weatherService.getCityForecastById(city_id).subscribe((res: any) => {
      console.log("res", res);
      res.list = res.list.filter(x => {
        let split_dt_txt = x.dt_txt.split(" ");
        if (split_dt_txt[1] === '21:00:00'){
          x['weekday'] = this.weekdays[new Date(x.dt_txt).getDay()];
          x['date'] = split_dt_txt[1];
          return x;
        }
      });
      this.city_forecast = res;
    }, error => {
      console.log("error", error);
    })
    // this.weatherService.getCityForecastById(city_id).subscribe((res: any) => {
    //   console.log("response for city forcast", res);
    //   this.city_forecast = res;
    // }, error => {
    //   console.log("error", error);
    // });
  }

}
